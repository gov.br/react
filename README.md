# Ambiente
## Levantar
```bash
docker compose up
```

## Manutenção
### Adicionar bibliotecas
```bash
docker compose exec web npm install package-name
```

### Atualizar bibliotecas
```bash
docker compose run web /bin/sh -c 'ncu'
```

### Recriar ambiente - quando houver mudanças no ambiente
```bash
docker compose down --rmi all -v
docker compose build --no-cache
docker compose up --force-recreate --build
```

### Acessar logs
```bash
docker compose logs -f web
```

### Acessar o terminal
```bash
docker compose run web /bin/sh
docker exec -it react-web-1 /bin/sh
```

### Acessar um container temporário (usar quando não levar o oficial)
CMD
```bash
docker run --rm -it -v %cd%:/app -t node:18-alpine /bin/sh --login
```

PowerShell
```bash
docker run --rm -it -v ${PWD}:/app -t node:18-alpine /bin/sh --login
```

Linux
```bash
docker run --rm -it -v "$(pwd)":/app -t node:18-alpine /bin/sh --login
```

# Gerar Classes API com Swagger
openapi -i http://127.0.0.1/docs/api-docs.json -o src/services/openapi

## DESING SYSTEM GOV.BR
# URL e Instalação
url: https://www.gov.br/ds/home
npm install @govbr-ds/core
npm install @fortawesome/fontawesome-free