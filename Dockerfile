FROM node:18-alpine

# Create and define the node_modules's cache directory.
WORKDIR /cache
COPY package*.json .
RUN npm install --verbose
RUN npm install -g npm@9.6.1 npm-check-updates

WORKDIR /app
ENV PATH /app/node_modules/.bin:$PATH
# Copy app files
COPY . .

#COPY entrypoint.sh /entrypoint.sh
#RUN chmod +x /entrypoint.sh
#ADD . .
#ENTRYPOINT ["/entrypoint.sh"]

# Expose port
EXPOSE 3000 5173

#CMD ["npm", "run", "dev", "--host"]