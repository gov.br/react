import { CircularProgress } from '@material-ui/core'
import React from 'react'
import { useEstiloLoadingCircular } from './LoadingCircularEstilo'
const LoadingCircular = (props) => {
    // const { root, bottom, top, circle } = useEstiloLoadingCircular()
    return (
        <div>
            <CircularProgress
                variant="determinate"
                size={40}
                thickness={4}
                {...props}
                value={100}
            />
            <CircularProgress
                variant="indeterminate"
                disableShrink
                classes={{
                    circle: circle,
                }}
                size={40}
                thickness={4}
                {...props}
            />
        </div>
    )
}
export default LoadingCircular