import { makeStyles } from "@material-ui/core";
import { useStorage } from "../../services/contextos/StorageContexto"

export const useEstiloLoadingCircular = _ => {
    const { modulo } = useStorage()
    const useStylesFacebook = makeStyles((tema) => ({
        root: {
            position: 'relative',
        },
        bottom: {
            color: tema.palette.grey[tema.palette.type === 'light' ? 200 : 700],
        },
        top: {
            color: (tema.palette.modulo.default.principal),
            animationDuration: '550ms',
            position: 'absolute',
            left: 0,
        },
        circle: {
            strokeLinecap: 'round',
        },
    }));

    return useStylesFacebook()
}
