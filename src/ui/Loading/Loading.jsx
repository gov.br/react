import React from 'react'
import { useEstiloLoading } from './LoadingEstilo'
const Loading = () => {
    const LodingPersonalizado = useEstiloLoading()
    return (
        <LodingPersonalizado />
    )
}
export default Loading