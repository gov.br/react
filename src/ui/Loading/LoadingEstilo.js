import { LinearProgress, withStyles } from "@material-ui/core"
import { useStorage } from "../../services/contextos/StorageContexto"

export const useEstiloLoading = _ => {
    const { modulo } = useStorage()
    return withStyles((tema) => ({
        root: {
            height: 5,
            borderRadius: 5,
        },
        colorPrimary: {
            backgroundColor: tema.palette.grey[tema.palette.type === 'light' ? 200 : 700],
        },
        bar: {
            borderRadius: 5,
            backgroundColor: (modulo ? tema.palette.modulo[modulo].principal : tema.palette.modulo.default.principal),
        },
    }))(LinearProgress);
}
