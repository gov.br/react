import { Outlet } from 'react-router-dom';

import BarraTopo from '../BarraTopo/BarraTopo';

import styles from './Conteudo.module.css'
import Migalha from '../Migalha/migalha';
import ModalProvider from '../../services/contextos/ModalContexto';
import StorageProvider from '../../services/contextos/StorageContexto';
import Modal from '../Modal/Modal';
import MenuLateral from '../Menu/MenuLateral';

const Conteudo = () => {
    return (
        <>
            <ModalProvider>
                <StorageProvider>
                    <Modal />
                    <BarraTopo />
                    <MenuLateral />
                    <main className={styles.content}>
                        <Outlet />
                    </main>
                </StorageProvider>
            </ModalProvider>
        </>
    )

}

export default Conteudo;