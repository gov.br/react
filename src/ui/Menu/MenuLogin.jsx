import { Button, Menu, MenuItem } from '@material-ui/core';
import { useState } from 'react';
import { useStorage } from '../../services/contextos/StorageContexto';
import { Link, Navigate, useNavigate } from 'react-router-dom';
import Fade from "@material-ui/core/Fade"

const MenuLogin = () => {
  const [anchorEl, setAnchorEl] = useState(null);
  const { token, nomeUsuario } = useStorage();
  const navigate = useNavigate();

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const handleSair = async () => {
    localStorage.clear()
    handleClose()
    navigate("/");
    document.location.reload(true)
  }

  return (
    <>
      {!token ?
        <Link to='/'>
          <i className="fas fa-user" aria-hidden="true"></i><span className="d-sm-inline">Entrar</span>
        </Link>
        :
        <button className="br-sign-in small" type="button" data-trigger="login" onClick={handleClick}>
          <i className="fas fa-user" aria-hidden="true"></i><span className="d-sm-inline">{token ? nomeUsuario : 'Entrar'}</span>
        </button>
      }
      <Menu
        id="fade-menu"
        MenuListProps={{
          'aria-labelledby': 'fade-button',
        }}
        anchorEl={anchorEl}
        keepMounted
        open={Boolean(anchorEl)}
        onClose={handleClose}
        TransitionComponent={Fade}
        style={{ top: '45px' }}
      >
        {token && (
          <MenuItem style={{ width: '160px' }} onClick={handleSair}>
            <div className="header-subtitle">Sair</div>
          </MenuItem>
        )}
      </Menu>
    </>
  )
}
export default MenuLogin;