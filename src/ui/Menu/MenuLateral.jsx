import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import { Link } from 'react-router-dom';
import { Divider } from '@material-ui/core';
import { useStorage } from '../../services/contextos/StorageContexto';

const drawerWidth = 200;

const useStyles = makeStyles((theme) => ({
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
    zIndex: 2,
    marginTop: 50,
  },
  drawerPaper: {
    width: drawerWidth,
    marginTop: 50,
  },
  toolbar: theme.mixins.toolbar,
}));

function MenuLateral() {
  const { token } = useStorage();
  const classes = useStyles();

  return (
    token && (
      <div style={{
        display: 'flex',
        zIndex: 3,
      }}>
        <Drawer
          className={classes.drawer}
          variant="permanent"
          classes={{
            paper: classes.drawerPaper,
          }}
          anchor="left"
          sx={{
            width: drawerWidth,
            flexShrink: 0,
            [`& .MuiDrawer-paper`]: { width: drawerWidth, boxSizing: 'border-box' },
          }}
        >
          <div className={classes.toolbar} />
          <List style={{
            marginTop: 25
          }}>
            <Divider />
            {/* {['Inbox', 'Starred', 'Send email', 'Drafts'].map((text, index) => (
            <ListItem button key={text}>
            <ListItemIcon>{index % 2 === 0 ? <InboxIcon /> : <MailIcon />}</ListItemIcon>
            <ListItemText primary={text} />
            </ListItem>
          ))} */}
            <ListItem button component={Link} to="/cadastrar">
              <ListItemIcon><i class="fa-solid fa-inbox"></i></ListItemIcon>
              <ListItemText primary={"Cadastrar"} />
            </ListItem>
            <ListItem button component={Link} to="/listar">
              <ListItemIcon><i class="fa-solid fa-plus"></i></ListItemIcon>
              <ListItemText primary={"Listar"} />
            </ListItem>
            <Divider />
          </List>
        </Drawer>
      </div>
    )
  );
}

export default MenuLateral;