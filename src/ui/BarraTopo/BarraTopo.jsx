import React from 'react';
import { useStorage } from '../../services/contextos/StorageContexto';
import './BarraTopo.css';
import MenuLogin from '../Menu/MenuLogin';

const BarraTopo = () => {
  const { nomeUsuario } = useStorage();

  return (
    <header className="br-header mb-4" id="header" data-sticky="data-sticky"
      style={{
        position: "fixed",
        left: 0,
        top: 0,
        right: 0,
        width: '100%',
        zIndex: 100
      }}>
      <div className="container-fluid">
        <div className="header-top">
          <div className="header-logo">
            <span className="br-divider vertical"></span>
            <div className="header-sign">Protótipo</div>
          </div>
          <div className="header-actions">
            <div className="header-login">
              <div className="header-sign-in">
                <MenuLogin/>
              </div>
              <div className="header-avatar"></div>
            </div>
          </div>
        </div>
        <div className="header-bottom">
          <div className="header-menu">
            <div className="header-menu-trigger" id="header-navigation">
            </div>
            <div className="header-info">
              <div className="header-title">Protótipo de Projeto Projetado</div>
              <div className="header-subtitle">Feito por Projetadores</div>
            </div>
          </div>
        </div>
      </div>
    </header>
  );
};

export default BarraTopo;