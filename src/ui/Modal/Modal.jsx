import React, { useState } from 'react'
import { Dialog, DialogActions, DialogContent, DialogTitle, Divider, Grid } from "@material-ui/core"

import LoadingCircular from '../Loading/LoadingCircular'
import ModalBotoes from './ModalBotoes'

import { useModalContexto } from '../../services/contextos/ModalContexto'

import { useEstiloModal } from './ModalEstilo'

const Modal = (props) => {
    const { tituloModal, mensagemModal, modalAtivo, trocaModal, larguraModal, callback } = useModalContexto();
    const [loadingCallback, setLoadingCallback] = useState(false)

    console.log(tituloModal, mensagemModal, modalAtivo);
    const handleClose = () => {
        trocaModal()
    }

    return (
        <Dialog
            fullWidth
            disableEscapeKeyDown
            maxWidth={larguraModal || "sm"}
            onClose={handleClose}
            open={modalAtivo}
        >
            {loadingCallback ? (
                <Grid container>
                    <LoadingCircular />
                </Grid>
            ) : (
                <React.Fragment>
                    <DialogTitle onClose={handleClose} style={{ textTransform: "capitalize" }}>
                        {tituloModal}
                        <Divider />
                    </DialogTitle>
                    <DialogContent>
                        {mensagemModal}
                    </DialogContent>
                    <DialogActions>
                        <ModalBotoes setLoadingCallback={setLoadingCallback} trocaModal={trocaModal} callback={callback} />
                    </DialogActions>
                </React.Fragment>
            )
            }
        </Dialog>
    )
}

export default Modal