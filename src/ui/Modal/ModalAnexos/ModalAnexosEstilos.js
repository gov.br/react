import { makeStyles } from "@material-ui/core"
import { useStorage } from "../../../services/contextos/StorageContexto"

export const useEstiloAnexosModal = _ => {

    const { modulo } = useStorage()

    const useAnexosModalEstilo = makeStyles((tema) => ({
        root: {
            margin: 0,
            padding: tema.spacing(2),
        },
        botao: {
            color: (modulo ? tema?.palette?.modulo[modulo]?.principal : tema?.palette?.modulo?.default?.principal),
            "&:hover": {
                backgroundColor: "transparent"
            }
        },
        loadingEstilo: {
            padding: 50
        },
        closeButton: {
            position: 'absolute',
            right: tema.spacing(1),
            top: tema.spacing(1),
            color: (modulo ? tema.palette.modulo[modulo]?.principal : tema.palette?.modulo.default?.principal)
        },
        labelModulos: {
            marginBottom: '8px'
        },
        cardModal: {
            borderLeft: "5px solid " + (modulo ? tema.palette?.modulo[modulo]?.principal : tema.palette.modulo?.default?.principal)
        },
        iconeLock: {
            color: (modulo ? tema.palette.modulo[modulo].principal : tema.palette.modulo.default.principal),
            verticalAlign: 'baseline'
        },
        miniTexto: {
            color: 'rgba(0, 0, 0, 0.54)',
            padding: 0,
            fontSize: '15px',
            fontFamily: 'Roboto, Helvetica, Arial, sansSerif',
            fontWeight: 400,
            lineHeight: 1,
            letterSpacing: '0.00938em',
            margin: '0px',
            marginBottom: '3px',
            '&>b': {
                fontSize: '0.875rem'
            }
        },
        iconesContato: {
            fontSize: '60px',
            verticalAlign: 'middle',
            color: (modulo ? tema.palette.modulo[modulo].principal : tema.palette.modulo.default.principal),
            paddingRight: '20px'
        },
        cardModalContent: {
            paddingBottom: '16px !important'
        },
        textosContato: {
            fontWeight: 400,
            color: 'rgba(0, 0, 0, 0.54)',
        },
        linksContato: {
            textDecoration: 'none'
        },
        tituloModal: {
            fontSize: '23px'
        }
    }))
    return useAnexosModalEstilo()
}
