import React, { useEffect, useState, useRef } from 'react'
import Dialog from '@material-ui/core/Dialog'
import MuiDialogTitle from '@material-ui/core/DialogTitle'
import IconButton from '@material-ui/core/IconButton'
import CloseIcon from '@material-ui/icons/Close'
import Typography from '@material-ui/core/Typography'
import { useEstiloAnexosModal } from './ModalAnexosEstilos'
import DialogContent from "@material-ui/core/DialogContent"
import DialogActions from "@material-ui/core/DialogActions"
import { useModalAnexosContexto } from "../../../services/contextos/ModalAnexosContexto"
import Grid from "@material-ui/core/Grid"
import { TextField, Button, TableContainer, Paper, Table, TableHead, TableCell, TableRow, TableBody } from "@material-ui/core"
import { useRequisicoesAnexo } from "../../../services/requisicoesHttp/RequisicoesAnexo"
import { useFuncoesAuxiliares } from "../../../services/hooksAuxiliares/funcoesAuxiliares";
import { useOrgaoSchema } from '../../../components/admin/Orgao/OrgaoSchemas'
import { useHistory } from "react-router-dom";
import { useModalContexto } from "../../../services/contextos/ModalContexto";
import { AttachFile } from '@mui/icons-material'
import IconeLink from '../../IconeLink/IconeLink'

const DialogTitle = (props) => {
    const { onClose } = props
    const { closeButton, root, tituloModal } = useEstiloAnexosModal()
    return (
        <MuiDialogTitle disableTypography className={root}>
            <Typography variant="h6" className={tituloModal}>{'Anexos'}</Typography>
            {onClose ? (
                <IconButton aria-label="close" className={closeButton} onClick={onClose}>
                    <CloseIcon />
                </IconButton>
            ) : null}
        </MuiDialogTitle>
    )
}

const ModalAnexos = () => {

    const [loadingAnexo, setLoadingAnexo] = useState(false)
    const { modalAnexos, setModalAnexos, modalAnexosStatus } = useModalAnexosContexto()
    const { setModal } = useModalContexto()
    const { diligenciaSchema, diligenciaInicial } = useOrgaoSchema()
    const [diligencia, setDiligencia] = useState(diligenciaInicial)
    const [edicao, setEdicao] = useState(1)
    const [anexo, setAnexo] = useState(null)
    const [upload, setUpload] = useState(0)
    const [loading, setLoading] = useState(false)
    const history = useHistory()
    const formRef = useRef();

    const { dataHora } = useFuncoesAuxiliares();

    const { getAnexoUsuario, getAnexoUnidade } = useRequisicoesAnexo()

    const handleClose = () => {
        setModalAnexos({
            anexos: null,
            status: false
        })
    }

    useEffect(() => {
        if (modalAnexos) {

        }
    }, [modalAnexos])

    return (
        <Dialog fullWidth aria-labelledby="customized-dialog-title" open={modalAnexosStatus} maxWidth='lg' backdrop="static">
            <DialogTitle id="customized-dialog-title" onClose={handleClose}></DialogTitle>
            <DialogContent dividers className='pb-0'>
                <Grid item sx={12} className="pt-2 mb-5">
                    <TableContainer component={Paper}>
                        <Table sx={{ minWidth: 650 }} size="small" aria-label="a dense table">
                            <TableHead style={{ backgroundColor: '#CDC9C9' }} className="text-white">
                                <TableRow>
                                    <TableCell align="left" style={{ width: '15%' }}>DATA DE CRIAÇÃO</TableCell>
                                    <TableCell align="left" style={{ width: '75%' }}>NOME DO ARQUIVO</TableCell>
                                    <TableCell align="center" style={{ width: '10%' }}>AÇÕES</TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                <TableRow
                                    key={modalAnexos?.anexoSujeito?.cod_anexo_usuario}
                                    sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                                >
                                    <TableCell scope="row">
                                        {dataHora(modalAnexos?.anexoSujeito?.dat_criacao_anexo)}
                                    </TableCell>
                                    <TableCell scope="row">
                                        {modalAnexos?.anexoSujeito?.label}
                                    </TableCell>
                                    <TableCell align="center">
                                        <IconeLink onClick={async () => {
                                            if (modalAnexos?.anexoSujeito?.cod_anexo_usuario) {
                                                setLoadingAnexo(true)
                                                const response = await getAnexoUsuario(modalAnexos?.anexoSujeito?.cod_anexo_usuario)

                                                var blob = new Blob([response.data], {
                                                    type: 'application/pdf'
                                                });
                                                var url = window.URL.createObjectURL(blob)
                                                window.open(url);
                                                setLoadingAnexo(false)
                                            } else {
                                                setLoadingAnexo(true)
                                                const response = await getAnexoUnidade(modalAnexos?.anexoSujeito?.cod_anexo_orgao)

                                                var blob = new Blob([response.data], {
                                                    type: 'application/pdf'
                                                });
                                                var url = window.URL.createObjectURL(blob)
                                                window.open(url);
                                                setLoadingAnexo(false)
                                            }
                                        }} titulo="Anexo">
                                            <AttachFile></AttachFile>
                                        </IconeLink>
                                    </TableCell>
                                </TableRow>
                            </TableBody>
                        </Table>
                    </TableContainer>
                </Grid>
            </DialogContent>
        </Dialog>
    );
};
export default ModalAnexos