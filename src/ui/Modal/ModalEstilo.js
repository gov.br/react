import { makeStyles } from "@material-ui/core"

export const useEstiloModal = _ => {

    const useEstilo = makeStyles((tema) => {

        return ({
            botao: {
                "&:hover": {
                    backgroundColor: "transparent"
                }
            },
            loadingEstilo: {
                padding: 50
            }
        })
    })

    return useEstilo()
}
