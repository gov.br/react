import React from 'react';

import { Button } from '@material-ui/core';

import { useModalContexto } from '../../services/contextos/ModalContexto';

import { useEstiloModal } from './ModalEstilo';

const ModalBotoes = (props) => {
    const { trocaModal, callback, botoes } = useModalContexto()
    const { setLoadingCallback } = props
    const handleClose = () => {
        trocaModal()
    }
    return botoes ? botoes : callback ? (
        <React.Fragment>
            <Button onClick={handleClose}>
                Não
            </Button>
            <Button onClick={async () => {
                setLoadingCallback(true)
                await callback()
                setLoadingCallback(false)
            }}>
                Sim
            </Button>
        </React.Fragment>
    ) : (
        <Button onClick={handleClose}>
            Ok
        </Button>
    )
}

export default ModalBotoes
