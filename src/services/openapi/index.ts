/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
export { ApiError } from './core/ApiError';
export { CancelablePromise, CancelError } from './core/CancelablePromise';
export { OpenAPI } from './core/OpenAPI';
export type { OpenAPIConfig } from './core/OpenAPI';

export type { Exemplo } from './models/Exemplo';
export type { Exemplos } from './models/Exemplos';

export { AuthService } from './services/AuthService';
export { ExemploService } from './services/ExemploService';
