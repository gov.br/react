/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { CancelablePromise } from '../core/CancelablePromise';
import { OpenAPI } from '../core/OpenAPI';
import { request as __request } from '../core/request';

export class AuthService {

    /**
     * Post your email and password and we will return a token. Use the token in the 'Authorization' header like so 'Bearer YOUR_TOKEN'
     * @param requestBody The Token Request
     * @returns any OK
     * @throws ApiError
     */
    public static postAuthLogin(
requestBody: {
email?: string;
password?: string;
},
): CancelablePromise<{
email?: string;
password?: string;
}> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/auth/login',
            body: requestBody,
            mediaType: 'application/json',
            errors: {
                422: `The provided credentials are incorrect.`,
            },
        });
    }

    /**
     * Informações do usuário proprietário do token
     * Informações do usuário proprietário do token
     * @returns any OK
     * @throws ApiError
     */
    public static me(): CancelablePromise<{
email?: string;
password?: string;
}> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/auth/me',
            errors: {
                422: `The provided credentials are incorrect.`,
            },
        });
    }

}
