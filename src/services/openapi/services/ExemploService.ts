/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { Exemplo } from '../models/Exemplo';

import type { CancelablePromise } from '../core/CancelablePromise';
import { OpenAPI } from '../core/OpenAPI';
import { request as __request } from '../core/request';

export class ExemploService {

    /**
     * Listar exemplos
     * Retorna a lista de exemplos
     * @returns Exemplo Success
     * @throws ApiError
     */
    public static list(): CancelablePromise<Exemplo> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/exemplos',
            errors: {
                422: `Missing Data`,
            },
        });
    }

    /**
     * Criar exemplo
     * Criar exemplo
     * @param requestBody Criar exemplo
     * @returns any Created
     * @throws ApiError
     */
    public static store(
requestBody: {
data?: Exemplo;
},
): CancelablePromise<{
data?: Exemplo;
}> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/exemplos',
            body: requestBody,
            mediaType: 'application/json',
            errors: {
                422: `Validation exception`,
            },
        });
    }

    /**
     * Visualizar exemplo
     * Visualizar exemplo
     * @param exemploId Id do Exemplo
     * @returns any Successful operation
     * @throws ApiError
     */
    public static show(
exemploId: number,
): CancelablePromise<{
data?: Exemplo;
}> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/exemplos/{exemplo_id}',
            path: {
                'exemplo_id': exemploId,
            },
            errors: {
                404: `Exemplo não encontrado`,
            },
        });
    }

    /**
     * Excluir um exemplo
     * Remove o registro do exemplo
     * @param exemploId Id do Exemplo
     * @returns void 
     * @throws ApiError
     */
    public static delete(
exemploId: number,
): CancelablePromise<void> {
        return __request(OpenAPI, {
            method: 'DELETE',
            url: '/exemplos/{exemplo_id}',
            path: {
                'exemplo_id': exemploId,
            },
            errors: {
                404: `Exemplo not found`,
            },
        });
    }

    /**
     * Atualizar exemplo
     * Atualizar exemplo
     * @param exemploId Id do Exemplo
     * @param requestBody Identificador do exemplo
     * @returns void 
     * @throws ApiError
     */
    public static update(
exemploId: number,
requestBody: {
data?: Exemplo;
},
): CancelablePromise<void> {
        return __request(OpenAPI, {
            method: 'PATCH',
            url: '/exemplos/{exemplo_id}',
            path: {
                'exemplo_id': exemploId,
            },
            body: requestBody,
            mediaType: 'application/json',
        });
    }

}
