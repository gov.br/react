/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { dateTime } from './dateTime';

/**
 * Exemplo schema
 */
export type Exemplo = {
    /**
     * identificador do exmeplo
     */
    id?: number;
    /**
     * nome do exemplo
     */
    nome?: string;
    /**
     * quanttidade de exemplos
     */
    quantidade?: number;
    /**
     * valor de exemplos
     */
    valor_real?: number;
    dt_exemplo?: dateTime;
    created_at?: dateTime;
    updated_at?: dateTime;
};
