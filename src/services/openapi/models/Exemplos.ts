/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { Exemplo } from './Exemplo';

export type Exemplos = {
    data?: Array<Exemplo>;
};
