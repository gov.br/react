import React, { useContext, useReducer } from "react"
import {
    StorageReducer,
    estadoInicial,
    SET_MODULO,
    SET_NOME_USUARIO,
    SET_COD_USUARIO,
    SET_TOKEN,
} from "./reducers/StorageReducer"
import { OpenAPI } from "../openapi/core/OpenAPI"

const StorageContexto = React.createContext()

export default function StorageProvider({ children }) {
    const [state, dispatch] = useReducer(StorageReducer, estadoInicial)

    OpenAPI.TOKEN = state.token_jwt;

    return (
        <StorageContexto.Provider value={{
            modulo: state.modulo,
            alteraModulo: modulo => dispatch({ type: SET_MODULO, payload: modulo }),
            nomeUsuario: state.nome_usuario,
            alteraNomeUsuario: nomeUsuario => dispatch({ type: SET_NOME_USUARIO, payload: nomeUsuario }),
            codUsuario: state.cod_usuario,
            alteraCodUsuario: codUsuario => dispatch({ type: SET_COD_USUARIO, payload: codUsuario }),
            token: state.token_jwt,
            alteraToken: token => dispatch({ type: SET_TOKEN, payload: token })
        }}>
            {children}
        </StorageContexto.Provider >
    )
}

export const useStorage = () => {
    const {
        modulo,
        alteraModulo,
        nomeUsuario,
        alteraNomeUsuario,
        codUsuario,
        alteraCodUsuario,
        token,
        alteraToken,
    } = useContext(StorageContexto)
    return {
        modulo,
        alteraModulo,
        nomeUsuario,
        alteraNomeUsuario,
        codUsuario,
        alteraCodUsuario,
        token,
        alteraToken,
    }
}