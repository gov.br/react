import {
    setModulo,
    setNomeUsuario,
    setCodUsuario,
    setToken,
} from "./actions/StorageActions" // ações do contexto

//Local Storage (navegador)
const getItemStorage = key => localStorage.getItem(key)

const MODULO_KEY = "modulo"
const NOME_USUARIO_KEY = "nome_usuario"
const COD_USUARIO_KEY = "cod_usuario"
const TOKEN_KEY = "token_jwt"

export const estadoInicial = {
    [MODULO_KEY]: getItemStorage(MODULO_KEY) || "",
    [NOME_USUARIO_KEY]: getItemStorage(NOME_USUARIO_KEY) || "",
    [COD_USUARIO_KEY]: getItemStorage(COD_USUARIO_KEY) || "",
    [TOKEN_KEY]: getItemStorage(TOKEN_KEY) || "",
}

export const SET_MODULO = "altera_modulo"
export const SET_NOME_USUARIO = "altera_nome_usuario"
export const SET_COD_USUARIO = "altera_cod_usuario"
export const SET_TOKEN = "altera_token"

export const StorageReducer = (state, action) => {
    switch (action.type) {
        case SET_MODULO:
            return setModulo(state, action.payload, MODULO_KEY)
        case SET_NOME_USUARIO:
            return setNomeUsuario(state, action.payload, NOME_USUARIO_KEY)
        case SET_COD_USUARIO:
            return setCodUsuario(state, action.payload, COD_USUARIO_KEY)
        case SET_TOKEN:
            return setToken(state, action.payload, TOKEN_KEY)
        default:
            return state
    }
}