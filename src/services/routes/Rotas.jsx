import Login from '../../componentes/login/Login'

import {
    createBrowserRouter,
    RouterProvider
} from 'react-router-dom'
import Listar from '../../componentes/exemplo/Listar'
import Cadastrar from '../../componentes/exemplo/Cadastrar'
import Conteudo from '../../ui/Conteudo/Conteudo'
import Inicio from '../../componentes/exemplo/Inicio'

const Rotas = () => createBrowserRouter([
    {
        path: "/",
        element: <Conteudo />,
        children: [
            {
                path: "/",
                element: <Login />
            },
            {
                path: "/listar",
                element: <Listar />
            },
            {
                path: "/inicio",
                element: <Inicio />
            },
            {
                path: "/cadastrar",
                element: <Cadastrar />
            },
            {
                path: "/cadastrar/:id",
                element: <Cadastrar />
            }
        ]
    },
])

export default Rotas