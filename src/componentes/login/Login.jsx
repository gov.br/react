import { Link, useNavigate } from "react-router-dom";
import { useStorage } from "../../services/contextos/StorageContexto";
import { AuthService } from './../../services/openapi/services/AuthService'
import { useModalContexto } from "../../services/contextos/ModalContexto";
import { Formik, Form } from "formik"
import * as Yup from "yup";
import jwt_decode from "jwt-decode";
import { Card, CardContent, Grid, Input, Typography } from "@material-ui/core";
import { useState } from "react";

const Login = () => {
    const navigate = useNavigate();
    const { alteraToken, alteraNomeUsuario, alteraCodUsuario } = useStorage()
    const { setModal } = useModalContexto()

    const [showPassword, setShowPassword] = useState(false);

    const handleShowPassword = () => {
        setShowPassword(!showPassword);
    };

    const handleLogin = (values) => {
        AuthService.postAuthLogin({
            email: values.email,
            password: values.password
        }).then(r => {
            if (r) {
                const decrypt = jwt_decode(r.access_token)
                alteraToken(r.access_token)
                alteraCodUsuario(decrypt?.sub)
                alteraNomeUsuario(decrypt?.nome)
                return navigate("/listar");
            }
        }).catch(() => {
            setModal({
                titulo: "Pode não!",
                mensagem: `Não Autorizado!`
            })
            return navigate("/");
        })
    }

    return (
        <Formik
            initialValues={{
                email: "",
                password: "",
            }}
            validationSchema={Yup.object().shape({
                email: Yup.string()
                    .email()
                    .required("Required")
            })}
            onSubmit={handleLogin}
        >
            {({ values, handleChange, handleReset, handleSubmit, dirty, isSubmitting }) => (
                <Form>
                    <Grid container
                        direction="row"
                        spacing={3}>
                        <Grid item xs={12} lg={6}>
                            <br-card data-expanded="on">
                                <br-card-content slot="content">
                                    <br-sign-in
                                        type="primary"
                                        density="middle"
                                        label="Entrar com"
                                        image="{ 'url': 'https://www.gov.br/++theme++padrao_govbr/img/govbr-colorido-b.png', 'description': 'gov.br' }"
                                        entity="gov.br"
                                    ></br-sign-in>
                                </br-card-content>
                            </br-card>
                        </Grid>
                        <Grid item xs={8} lg={6}>
                            <Card variant="outlined">
                                <CardContent>
                                    <div className="br-input small input-button">
                                        <label htmlFor="input-login-small">Logar</label>
                                        <input
                                            id="email"
                                            type="e-mail"
                                            placeholder="Digite seu e-mail"
                                            value={values.email}
                                            onChange={handleChange}
                                        />
                                        <button className="br-button" type="button" aria-label="Botão de ação"><i className="fas fa-arrow-right" aria-hidden="true"></i>
                                        </button>
                                    </div>
                                    <div className="br-input input-button">
                                        <label htmlFor="input-password">Senha</label>
                                        <input
                                            id="password"
                                            type={showPassword ? 'text' : 'password'}
                                            placeholder="Digite sua senha"
                                            value={values.password}
                                            onChange={handleChange}
                                        />
                                        <button className='br-button' type="button" aria-label={showPassword ? "Mostrar Senha" : "Ocultar Senha"} onClick={handleShowPassword}>
                                            {showPassword ? <i className="fas fa-eye-slash" aria-hidden="false"></i> : <i class="fa-solid fa-eye"></i>}
                                        </button>
                                    </div>
                                    <div className="card-footer">
                                        <button
                                            type="clear"
                                            className='br-button secondary mr-3'
                                            onClick={handleReset}
                                            disabled={!dirty || isSubmitting}
                                        >
                                            Reset
                                        </button>
                                        <button type="submit" className='br-button primary' disabled={isSubmitting}>
                                            Submit
                                        </button>
                                        <Typography>
                                            <Link to='/inicio'>
                                                Inicio Protótipo
                                            </Link>
                                        </Typography>
                                    </div>
                                </CardContent>
                            </Card>
                        </Grid>
                    </Grid>
                </Form>
            )}
        </Formik>
    );
}

export default Login;