import React, { useEffect, useState } from 'react'
import { BrButton } from '@govbr-ds/react-components'
import { useNavigate, useParams } from 'react-router-dom';
import { ExemploService } from './../../services/openapi/services/ExemploService'

const Cadastrar = () => {
  const [filtro, setFiltro] = useState({});
  const navigate = useNavigate();
  const { id } = useParams();

  /**
   * Atualiza os componentes de text.
   * @param {*} e 
 */
  const handleChange = e => {
    setFiltro({
      ...filtro,
      [e.target.name]: e.target.value
    });
  }

  const handleSubmit = () => {
    if (id) {
      ExemploService.update(id, { data: filtro }).then(r => {
        setFiltro({
          nome: '',
          dt_exemplo: '',
          valor_real: '',
        })
        return navigate("/listar");

      }).catch(() => {
        alert('Deu errado@')
      });
    } else {
      ExemploService.store({ data: filtro }).then(r => {
        setFiltro({
          nome: '',
          dt_exemplo: '',
          valor_real: '',
        })
        return navigate("/listar");

      }).catch(() => {
        alert('Deu errado@')
      });
    }
  }

  const fetchDados = async () => {
    if (id) {
      const { data } = await ExemploService.show(id);
      setFiltro({
        nome: data?.nome,
        dt_exemplo: data?.dt_exemplo,
        valor_real: data?.valor_real,
      })
    }
  }

  useEffect(() => {
    fetchDados()
  }, [])

  return (
    <>
      <h1>{id ? `Editar sujeito ${id}` : 'Cadastrar'}</h1>
      <div className="br-input">
        <label>Nome</label>
        <div className="input-group">
          <div className="input-icon">
            <i className="fas fa-user" />
          </div>
          <input id="input-icon" name="nome" onChange={handleChange} type="text" value={filtro?.nome} placeholder="Digite seu nome" />
        </div>
      </div>
      <div className="br-input">
        <label>Data</label>
        <div className="input-group">
          <div className="input-icon">
            <i className="fas fa-date" />
          </div>
          <input id="input-icon" name="dt_exemplo" onChange={handleChange} type="date" value={filtro?.dt_exemplo} placeholder="Digite a data" />
        </div>
      </div>
      <div className="br-input">
        <label>Value</label>
        <div className="input-group">
          <div className="input-icon">
            <i className="fas fa-user" />
          </div>
          <input id="input-icon" name="valor_real" onChange={handleChange} type="number" value={filtro?.valor_real} placeholder="Digite o valor" />
        </div>
      </div>
      <div className="p-3">
        <BrButton className="br-button block secondary mb-3" type="button" onClick={handleSubmit}>{id ? 'Atualizar' : 'Cadastrar'}
        </BrButton>
      </div>
    </>
  )
}

export default Cadastrar