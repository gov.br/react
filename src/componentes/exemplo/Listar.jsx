import React, { useEffect, useState } from 'react'
import { Link, useNavigate } from 'react-router-dom'
import { ExemploService } from './../../services/openapi/services/ExemploService'
import { useFuncoesAuxiliares } from '../../services/hooksAuxiliares/funcoesAuxiliares'
import { Button } from '@material-ui/core'
import { useModalContexto } from '../../services/contextos/ModalContexto'

const Listar = () => {
  const navigate = useNavigate();

  const [dados, setDados] = useState();
  const { formataDataFront } = useFuncoesAuxiliares();
  const { setModal } = useModalContexto()

  const fetchDados = async () => {
    const { data } = await ExemploService.list();
    setDados(data);
  }

  const handleLogin2 = () => {
    setModal({
      titulo: "Opa estou testando!",
      mensagem: `Você pode ou não pode fazer isso!`
    })
  }

  const handleExcluir = async (id) => {
    await ExemploService.delete(id).then(r => {
      alert("Tem certeza? Vai excluir do mesmo jeito")
      return fetchDados();
    }).catch(() => {
      alert('Deu errado@')
    });
    return navigate("/listar");
  }

  useEffect(() => {
    fetchDados();
  }, [])

  return (
    <>
      <div className="main-content pl-sm-3 mt-4" id="main-content">
        <h1>Listar</h1>
        <div className="row">
          <div className="br-table large" title="Tabela irregular 1">
            <table>
              <thead>
                <tr>
                  <th className="border-bottom" scope="col">Nome</th>
                  <th className="border-bottom border-left" scope="col">Data</th>
                  <th className="border-bottom border-left" colSpan="1" scope="colgroup">Value</th>
                  <th className="border-bottom border-left" colSpan="3" scope="colgroup">Ação</th>
                </tr>
              </thead>
              <tbody>
                {dados && dados.map((d) => {
                  return (
                    <>
                      <tr key={d.id}>
                        <td>{d.nome}</td>
                        <td>{formataDataFront(d.created_at)}</td>
                        <td>{d.valor_real}</td>
                        <a href={`/cadastrar/${d.id}`}>
                          <span className="icon">
                            <i className="fa-solid fa-pen-to-square" aria-hidden="true"></i>
                          </span>
                        </a>
                        <a onClick={() => handleExcluir(d.id)}>
                          <span className="icon">
                            <i className="fa-solid fa-trash"></i>
                          </span>
                        </a>
                      </tr>
                    </>
                  )
                })}
              </tbody>
            </table>
          </div>
        </div>
            {/* <Button onClick={handleLogin2}>Olá</Button> */}
      </div>
    </>
  )
}

export default Listar