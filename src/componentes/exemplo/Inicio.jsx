
const Inicio = () => {

  return (
    <div class="template-base">
      <main class="d-flex flex-fill mb-5" id="main">
        <div class="container-fluid d-flex">
          <div class="row">
            <div class="br-menu push" id="main-navigation">
              <div class="menu-container">
                <div class="menu-panel">
                  <div class="menu-header">
                    <div class="menu-title">
                      <span>Identificação do site ou Sistema</span></div>
                    <div class="menu-close">
                      <button class="br-button circle" type="button" aria-label="Fechar o menu" data-dismiss="menu"><i class="fas fa-times" aria-hidden="true"></i>
                      </button>
                    </div>
                  </div>
                  <nav class="menu-body">
                    <div class="menu-folder"><a class="menu-item" href=""><span class="icon"><i class="fas fa-bell" aria-hidden="true"></i></span><span class="content">Nível 1</span></a>
                      <ul>
                        <li><a class="menu-item" href=""><span class="icon"><i class="fas fa-heart" aria-hidden="true"></i></span><span class="content">Nível 2</span></a></li>
                        <li><a class="menu-item" href=""><span class="icon"><i class="fas fa-address-book" aria-hidden="true"></i></span><span class="content">Nível 2</span></a>
                          <ul>
                            <li><a class="menu-item" href=""><span class="icon"><i class="fas fa-book" aria-hidden="true"></i></span><span class="content">Nível 3</span></a></li>
                            <li><a class="menu-item" href=""><span class="icon"><i class="fas fa-tree" aria-hidden="true"></i></span><span class="content">Nível 3</span></a>
                              <ul>
                                <li><a class="menu-item" href=""><span class="content">Nível 4</span></a></li>
                                <li><a class="menu-item" href=""><span class="content">Nível 4</span></a></li>
                                <li><a class="menu-item" href=""><span class="content">Nível 4</span></a></li>
                              </ul>
                            </li>
                            <li><a class="menu-item" href=""><span class="icon"><i class="fas fa-moon" aria-hidden="true"></i></span><span class="content">Nível 3</span></a></li>
                          </ul>
                        </li>
                        <li><a class="menu-item" href=""><span class="icon"><i class="fas fa-archive" aria-hidden="true"></i></span><span class="content">Nível 2</span></a></li>
                      </ul>
                    </div>
                    <div class="menu-folder"><a class="menu-item" href=""><span class="icon"><i class="fas fa-bell" aria-hidden="true"></i></span><span class="content">Nível 1</span></a>
                      <ul>
                        <li><a class="menu-item" href=""><span class="icon"><i class="fas fa-heart" aria-hidden="true"></i></span><span class="content">Nível 2</span></a></li>
                        <li><a class="menu-item" href=""><span class="icon"><i class="fas fa-address-book" aria-hidden="true"></i></span><span class="content">Nível 2</span></a>
                          <ul>
                            <li><a class="menu-item" href=""><span class="icon"><i class="fas fa-book" aria-hidden="true"></i></span><span class="content">Nível 3</span></a></li>
                            <li><a class="menu-item" href=""><span class="icon"><i class="fas fa-tree" aria-hidden="true"></i></span><span class="content">Nível 3</span></a>
                              <ul>
                                <li><a class="menu-item" href=""><span class="content">Nível 4</span></a></li>
                                <li><a class="menu-item" href=""><span class="content">Nível 4</span></a></li>
                                <li><a class="menu-item" href=""><span class="content">Nível 4</span></a></li>
                              </ul>
                            </li>
                            <li><a class="menu-item" href=""><span class="icon"><i class="fas fa-moon" aria-hidden="true"></i></span><span class="content">Nível 3</span></a></li>
                          </ul>
                        </li>
                        <li><a class="menu-item" href=""><span class="icon"><i class="fas fa-archive" aria-hidden="true"></i></span><span class="content">Nível 2</span></a></li>
                      </ul>
                    </div>
                    <div class="menu-folder"><a class="menu-item" href=""><span class="icon"><i class="fas fa-bell" aria-hidden="true"></i></span><span class="content">Nível 1</span></a>
                      <ul>
                        <li><a class="menu-item" href=""><span class="icon"><i class="fas fa-heart" aria-hidden="true"></i></span><span class="content">Nível 2</span></a></li>
                        <li><a class="menu-item" href=""><span class="icon"><i class="fas fa-address-book" aria-hidden="true"></i></span><span class="content">Nível 2</span></a>
                          <ul>
                            <li><a class="menu-item" href=""><span class="icon"><i class="fas fa-book" aria-hidden="true"></i></span><span class="content">Nível 3</span></a></li>
                            <li><a class="menu-item" href=""><span class="icon"><i class="fas fa-tree" aria-hidden="true"></i></span><span class="content">Nível 3</span></a>
                              <ul>
                                <li><a class="menu-item" href=""><span class="content">Nível 4</span></a></li>
                                <li><a class="menu-item" href=""><span class="content">Nível 4</span></a></li>
                                <li><a class="menu-item" href=""><span class="content">Nível 4</span></a></li>
                              </ul>
                            </li>
                            <li><a class="menu-item" href=""><span class="icon"><i class="fas fa-moon" aria-hidden="true"></i></span><span class="content">Nível 3</span></a></li>
                          </ul>
                        </li>
                        <li><a class="menu-item" href=""><span class="icon"><i class="fas fa-archive" aria-hidden="true"></i></span><span class="content">Nível 2</span></a></li>
                      </ul>
                    </div><a class="menu-item divider" href=""><span class="icon"><i class="fas fa-bell" aria-hidden="true"></i></span><span class="content">Item de nível 1</span></a><a class="menu-item divider" href=""><span class="icon"><i class="fas fa-bell" aria-hidden="true"></i></span><span class="content">Item de nível 1</span></a>
                  </nav>
                  <div class="menu-footer d-sm-none">
                    <div class="menu-logos">
                      </div>
                    <div class="menu-links"><a href=""><span class="mr-1">Link externo 1</span><i class="fas fa-external-link-square-alt" aria-hidden="true"></i></a><a href=""><span class="mr-1">Link externo 2</span><i class="fas fa-external-link-square-alt" aria-hidden="true"></i></a></div>
                    <div class="social-network">
                      <div class="social-network-title">Redes Sociais</div>
                      <div class="d-flex"><a class="br-button circle" href="" aria-label="Compartilhar por Facebook"><i class="fab fa-facebook-f" aria-hidden="true"></i></a><a class="br-button circle" href="" aria-label="Compartilhar por Twitter"><i class="fab fa-twitter" aria-hidden="true"></i></a><a class="br-button circle" href="" aria-label="Compartilhar por Linkedin"><i class="fab fa-linkedin-in" aria-hidden="true"></i></a><a class="br-button circle" href="" aria-label="Compartilhar por Whatsapp"><i class="fab fa-whatsapp" aria-hidden="true"></i></a></div>
                    </div>
                    <div class="menu-info">
                      <div class="text-center text-down-01">Todo o conteúdo deste site está publicado sob a licença <strong>Creative Commons Atribuição-SemDerivações 3.0</strong></div>
                    </div>
                  </div>
                </div>
                <div class="menu-scrim" data-dismiss="menu" tabindex="0"></div>
              </div>
            </div>
            <div class="col mb-5">
              <div class="br-breadcrumb">
                <ul class="crumb-list">
                  <li class="crumb home"><a class="br-button circle" href=""><span class="sr-only">Página inicial</span><i class="fas fa-home"></i></a></li>
                  <li class="crumb"><i class="icon fas fa-chevron-right"></i><a href="">Tela Anterior</a>
                  </li>
                  <li class="crumb"><i class="icon fas fa-chevron-right"></i><a href="">Tela Anterior2</a>
                  </li>
                  <li class="crumb" data-active="active"><i class="icon fas fa-chevron-right"></i><span>Tela Atual</span>
                  </li>
                </ul>
              </div>
              <div class="main-content pl-sm-3 mt-4" id="main-content">
                <h1>Título h1</h1>
                <p>Parágrafo de exemplo <a href="">link de exemplo</a>.</p>
                <h2>Título h2</h2>                
                <div class="row">
                  <div class="col-auto">
                    <ul>
                      <li>item sem ordenação</li>
                      <li>item sem ordenação</li>
                    </ul>
                  </div>
                  <div class="col-auto">
                    <ol>
                      <li>item ordenado</li>
                      <li>item ordenado</li>
                    </ol>
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm">
                    <fieldset>
                      <legend>Nome Completo (Legend)</legend>
                      <div class="row">
                        <div class="col-md-7 mb-3">
                          <div class="br-input"><label for="name">Nome</label>
                            <input id="name" value="Fulano (input preenchido)" type="text" />
                          </div>
                        </div>
                        <div class="col-md-7 mb-3">
                          <div class="br-input">
                            <label for="surname">Sobrenome</label>
                            <input id="surname" type="text" placeholder="Placeholder" />
                          </div>
                        </div>
                      </div>
                    </fieldset>
                  </div>
                  <div class="col-sm">
                    <fieldset>
                      <legend>Outros Dados (Legend)</legend>
                      <div class="row">
                        <div class="col-md-7 mb-3">
                          <div class="br-input">
                            <label for="cpf">CPF</label>
                            <input id="cpf" type="text" placeholder="Placeholder" />
                          </div>
                        </div>
                        <div class="col-md-7 mb-3">
                          <div class="br-input input-button">
                            <label for="input-password">Senha</label>
                            <input id="input-password" type="password" placeholder="Digite sua senha" />
                            <button class="br-button" type="button" aria-label="Mostrar senha"><i class="fas fa-eye" aria-hidden="true"></i>
                            </button>
                          </div>
                        </div>
                      </div>
                    </fieldset>
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm d-flex">
                    <div class="br-card">
                      <div class="card-header">Card de exemplo 1</div>
                      <div class="card-content">Lorem ipsum dolor sit amet consectetur, adipisicing elit. Ducimus dignissimos, recusandae at accusamus autem error hic adipisci eligendi eum debitis dolores magnam quod sint doloribus omnis vitae placeat. Natus, minus.</div>
                    </div>
                  </div>
                  <div class="col-sm d-flex">
                    <div class="br-card">
                      <div class="card-header">Card de exemplo 2</div>
                      <div class="card-content">Lorem ipsum dolor, sit amet consectetur adipisicing elit. Veritatis culpa inventore mollitia vitae a nesciunt reiciendis incidunt, labore repudiandae voluptate nisi. Numquam illo est maiores doloribus recusandae rem inventore ea?</div>
                    </div>
                  </div>
                  <div class="col-sm d-flex">
                    <div class="br-card">
                      <div class="card-header">Card de exemplo 3</div>
                      <div class="card-content">Lorem ipsum, dolor sit amet consectetur adipisicing elit. Laboriosam cupiditate nihil facere autem dignissimos ex beatae labore? Fuga consequuntur consectetur voluptatum incidunt, tenetur, dolore magnam inventore hic iusto amet ipsa.</div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </main>
      <footer class="br-footer pt-3" id="footer">
        <div class="container-fluid">
          <div class="info">
            <div class="text-down-01 text-medium pb-3">Texto destinado a exibição de informações relacionadas à&nbsp;<strong><a href="">licença de uso</a>.</strong></div>
          </div>
        </div>
      </footer>
      <div class="br-cookiebar default d-none" tabindex="-1"></div>
    </div>
  )
}

export default Inicio